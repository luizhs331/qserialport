
#include <QCoreApplication>
#include <QSerialPort>
#include <QByteArray>
#include <QDebug>
#include <QTimer>

int main( int argc, char* argv[] ) {
    QCoreApplication a( argc, argv );

    QIODevice* p;
    QByteArray dados;

    QString comand = "\005";
    QString lengthComando = "1";

    // IF IT IS  SERIAL
    p = new QSerialPort( "/dev/ttyUSB0" );
    ( dynamic_cast<QSerialPort*>( p ) )->setBaudRate( 9600, QSerialPort::AllDirections );
    ( dynamic_cast<QSerialPort*>( p ) )->setParity( QSerialPort::Parity::NoParity );
    ( dynamic_cast<QSerialPort*>( p ) )->setStopBits( QSerialPort::StopBits::OneStop );
    ( dynamic_cast<QSerialPort*>( p ) )->setDataBits( QSerialPort::DataBits::Data8 );

    qDebug() << "[DOOR_OPEN]" << p->open( QIODevice::ReadWrite );

    auto* serialPort = dynamic_cast<QSerialPort*>( p );
    QObject::connect( serialPort, &QSerialPort::readyRead, [&]() {
        qDebug() << "[INFORMATION]" << p->readAll();
    } );

    QTimer qTimer;
    qTimer.setInterval( 3000 );
    QObject::connect( &qTimer, &QTimer::timeout, [&]() {
        p->write( comand.toStdString().c_str(), lengthComando.toStdString().length() );
    } );
    qTimer.start();

    return a.exec();
}
